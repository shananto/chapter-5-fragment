package com.binar.chapter5_fragment

interface FragmentListener {
    fun openFragmentSatu()
    fun openFragmentDua()
    fun openFragmentTiga()

    fun goToMenuActivity()
}