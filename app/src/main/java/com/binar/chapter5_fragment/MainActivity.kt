package com.binar.chapter5_fragment

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

class MainActivity : AppCompatActivity(), FragmentListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        openFragmentSatu()
    }

    fun commitFragment(fragment: Fragment){
        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
    }

    override fun openFragmentSatu() {
        val objectFragmentSatu = FragmentSatu()
        objectFragmentSatu.setListener(this)
        commitFragment(objectFragmentSatu)
    }

    override fun openFragmentDua() {
        val objectFragmentDua = FragmentDua()
        objectFragmentDua.setListener(this)
        commitFragment(objectFragmentDua)
    }

    override fun openFragmentTiga() {
        val objectFragmentTiga = FragmentTiga()
        objectFragmentTiga.setListener(this)
        commitFragment(objectFragmentTiga)
    }

    override fun goToMenuActivity() {
        val intentKeMenuActivity = Intent(this, MenuActivity::class.java)
        startActivity(intentKeMenuActivity)
    }
}